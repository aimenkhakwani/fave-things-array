# _Array Practice with Fave Things_

### _Array practice, August 19, 2016_

#### _**By Aimen Khakwani**_

## Description

_Takes user input and pushes it into arrays in JavaScript._

##Setup and Installation

* _Clone from BitBucket_
* _Run in browser_

## Technologies Used

_HTML, CSS, Bootstrap, jQuery, and JavaScript_

### License
Copyright (c) 2016 **_Aimen Khakwani**
